Test project for sbt-vuefy
============================

=> Taken from https://github.com/GIVESocialMovement/sbt-vuefy/tree/master/test-play-project and simplified somewhat

Run `yarn install` in order to install all the necessary packages.

mb: also run `npm install vue`!

`sbt run` to run locally. Try modify *.vue to see that the changes are recompiled.

`sbt stage` to package app for production deployment. Notice that the flag `-p` is used when compiling Vue components.
